package br.creche.diegokiyoshi;

import java.util.List;

public class Pai {
	
	private String nome;
	private String cpf;
	private List<Filho> listaFilho;
	
	public List<Filho> getListaFilho() {
		return listaFilho;
	}
	public void setListaFilho(List<Filho> listaFilho) {
		this.listaFilho = listaFilho;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
