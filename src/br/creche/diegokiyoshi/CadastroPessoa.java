package br.creche.diegokiyoshi;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class CadastroPessoa {
	
	public static void main(String[] args) {
		
		List<Pai> listaPai = new ArrayList<>();
		List<Filho> listaFilho = new ArrayList<>();
		Boolean desejaCadastrar = Boolean.TRUE;
		Scanner scanner = new Scanner(System.in);
		String dadosPessoaCadastrada;
	
		
//		while(desejaCadastrar) {
//			System.out.println("Bem vindo! Deseja cadastrar um pai? Digite 'Nome completo - CPF'. Para consultar, digite Consultar");
//			consultaCadastra = scanner.nextLine();
//			if(consultaCadastra.equalsIgnoreCase("Consultar")) {
//				desejaCadastrar = Boolean.FALSE;
//
//				for (Pessoa pessoa : listaPessoa) {
//					System.out.println(pessoa.getNome() + "-" + pessoa.getCpf());
//				}
//				
//			} else {
//				Pessoa pai = new Pessoa();
//				String[] retornaSplit = consultaCadastra.split("-", 2);
//				pai.setNome(retornaSplit[0]);
//				pai.setCpf(retornaSplit[1]);
//				listaPessoa.add(pai);
//			}
//		}
		/*
		 * ******************** O C�DIGO ABAIXO EST� TUDO SEPARADO ONDE H� O BLOCO DO
		 * CADASTRO SEPARADO DO BLOCO DE EXIBI��O DAS PESSOAS CADASTRADAS ***************
		 */		
		
		while(desejaCadastrar) {
			System.out.println("Bem vindo! Deseja cadastrar um pai? Digite 'Nome completo - CPF'. Para consultar, digite Consultar "
					+ "ou para cadastrar um filho, digite 'Nome do filho - idade do filho - CPF do pai cadastrado'");
			dadosPessoaCadastrada = scanner.nextLine();
			
			
			if(!dadosPessoaCadastrada.equalsIgnoreCase("Consultar")) {
				String[]retornaSplit = dadosPessoaCadastrada.split("-");
				

				if(retornaSplit.length == 2) {
					Pai pai = new Pai();
					pai.setNome(retornaSplit[0]);
					pai.setCpf(retornaSplit[1]);
					listaPai.add(pai);
					
					
					
				} else if(retornaSplit.length == 3) {
					Filho filho = new Filho();
					filho.setNome(retornaSplit[0]);
					filho.setIdade(retornaSplit[1]);
					filho.setCpf(retornaSplit[2]);
					listaFilho.add(filho);
					
				}
				


			} else {
				desejaCadastrar = Boolean.FALSE;
			}
		}
		
		
		for (Pai pais : listaPai) {
			
			String subStringCpf = pais.getCpf().substring(0, 10);
			String subStringCpfDigitoVerificador = pais.getCpf().substring(10);
			String subStringCpfFormatado = subStringCpf + "-" + subStringCpfDigitoVerificador;
			System.out.println("\n PAI: " + pais.getNome() + " - " + subStringCpfFormatado);

			for (Filho filhos : listaFilho) {

				if (pais.getCpf().equals(filhos.getCpf())) {
					pais.setListaFilho(listaFilho);

					System.out.println(
							"      FILHO: " + filhos.getNome() + " - " + filhos.getIdade() + " anos." );
					
				} 

			}
		}
	}
}
		


