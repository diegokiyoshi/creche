package br.creche.diegokiyoshi;

public class Filho extends Pai{
	
	private String idade;
	private Pai pai;

	public Pai getPai() {
		return pai;
	}

	public void setPai(Pai pai) {
		this.pai = pai;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}
	
	

}
